export interface Row {
  label: string;
  value: string | number;
  end?: string;
}

export interface CellValue {
  start: string;
  end: string;
}

export interface Cell {
  label: string;
  value: CellValue[];
}

export interface IDayPartingCell {
  start: string;
  day: string;
  end: string;
}
