import { useState } from 'react';

import { DayParting } from './components';
import { IDayPartingCell } from './interfaces';

function App() {
  const [cells, setCells] = useState<IDayPartingCell[]>([]);

  return (
    <>
      <DayParting
        cells={cells}
        handleCells={(value) => {
          setCells(value);
        }}
      />
    </>
  );
}

export default App;
