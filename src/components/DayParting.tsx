import { Children, FC, useCallback } from 'react';

import classNames from 'classnames';

import { Cell, CellValue, IDayPartingCell, Row } from '../interfaces';
import { days } from '../constants';
import { useDayParting } from './use-dayparting';

interface IDayPartingProps {
  cells: IDayPartingCell[];
  handleCells: (cells: IDayPartingCell[]) => void;
}

const DayParting: FC<IDayPartingProps> = ({ handleCells, cells }) => {
  const { hours } = useDayParting();

  const onClickCell = useCallback(
    (columnIndex: number, rowIndex: number, row: Row, column: CellValue) => {
      if (columnIndex === -1 || rowIndex === 0) return;

      const index = cells.findIndex(
        (cell) =>
          cell.day === row.value.toString() && column.start === cell.start
      );

      const newCells = [...cells];

      if (index === -1) {
        newCells.push({
          start: column.start,
          day: row.value.toString(),
          end: column.end,
        });
      }

      if (index > -1) newCells.splice(index, 1);

      handleCells(newCells);
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [cells]
  );

  const isChecked = useCallback(
    (dayValue: string, column: Cell, cell: CellValue) => {
      const hourValue =
        column.value.find((_value) => _value.start === cell.start)?.start || '';

      const item =
        cells.length > 0 &&
        cells.find((cell) => cell.day === dayValue && cell.start === hourValue);

      if (item) return true;

      return false;
    },
    [cells]
  );

  const Header: FC = () => {
    return (
      <thead>
        <tr>
          {hours.map((hour, index) => (
            <th key={index} scope="col" className="titles">
              <div>{hour.label}</div>
            </th>
          ))}
        </tr>
      </thead>
    );
  };

  return (
    <div className="container">
      <div className="actions">
        <h2>Setup Day Parting</h2>

        <button className="clr-btn" onClick={() => handleCells([])}>
          Clear all
        </button>
      </div>

      <div className="dayparting">
        <table>
          <Header />

          <tbody>
            {days.map((day, index) => {
              return (
                index > -1 && (
                  <tr key={index} data-item="true">
                    {hours.map((hour, _index) => {
                      return (
                        <td
                          key={_index}
                          className={classNames('', {
                            'p-3 titles': _index === 0,
                          })}
                        >
                          {_index === 0 ? (
                            day.label
                          ) : (
                            <ul className="parent-cell">
                              {Children.toArray(
                                hour.value.map((value) => (
                                  <li
                                    className={classNames('child-cell', {
                                      titles: _index === 0,
                                      active: isChecked(
                                        day.value.toString(),
                                        hour,
                                        value
                                      ),
                                      hover: !isChecked(
                                        day.value.toString(),
                                        hour,
                                        value
                                      ),
                                    })}
                                    onClick={() =>
                                      onClickCell(index, _index, day, value)
                                    }
                                  >
                                    {' '}
                                  </li>
                                ))
                              )}
                            </ul>
                          )}
                        </td>
                      );
                    })}
                  </tr>
                )
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export { DayParting };
