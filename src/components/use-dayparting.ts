import { useMemo } from 'react';
import { HOURS_IN_A_DAY, MINUTES_IN_AN_HOUR, STEP } from '../constants';
import { Cell } from '../interfaces';

const useDayParting = () => {
  const getLabel = (hour: number): string => {
    const minutes = '00';

    if (hour === 0) return `12:${minutes} am`;
    if (hour < 12) return `${hour}:${minutes} am`;
    if (hour > 12) return `${hour % 12}:${minutes} pm`;
    if (hour === 12) return `12:${minutes} pm`;

    return '';
  };

  const getEnd = (hour: number, min: number): string => {
    const minutes = min + STEP;
    if (minutes < MINUTES_IN_AN_HOUR)
      return `${hour.toString().padStart(2, '0')}:${minutes
        .toString()
        .padStart(2, '0')}`;

    if (minutes === MINUTES_IN_AN_HOUR && hour === HOURS_IN_A_DAY - 1)
      return '00:00';

    return `${(hour + 1).toString().padStart(2, '0')}:00`;
  };

  const generateHours = (): Cell[] => {
    const hours: Cell[] = [];

    for (let i = 0; i < HOURS_IN_A_DAY; i++) {
      const hour = {
        label: getLabel(i),
        value: [0, 1].map((counter) => ({
          start: `${i.toString().padStart(2, '0')}:${(counter * STEP)
            .toString()
            .padStart(2, '0')}`,
          end: getEnd(i, counter * STEP),
        })),
      };
      hours.push(hour);
    }

    hours.unshift({
      label: 'Time',
      value: [],
    });

    return hours;
  };

  const hours = useMemo(() => {
    return generateHours();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    generateHours,
    hours,
  };
};

export { useDayParting };
