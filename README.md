# DayParting Component

The DayParting component is a React component that allows users to set up day parting by selecting specific hours of the day. It provides an interactive table interface where users can toggle the selected hours for each day.

# Usage

Clone the repo -> hit yarn -> yarn dev
-OR using npm-
Clone the repo -> hit npm install -> npm run dev

# Example

I have put an example in src/App.tsx.

- Props
The DayParting component accepts the following props:

handleCells (required): A callback function to handle changes to the selected cells. It receives the updated array of selected cells as an argument.
cells (optional): An array representing the initially selected cells. Each cell object should have the following properties:
start: The start time of the selected hour.

day: The day value of the selected hour.
    + Map Days Array
    ['MONDAY',
    'TUESDAY',
    'WEDNESDAY',
    'THURSDAY',
    'FRIDAY',
    'SATURDAY',
    'SUNDAY']

end: The end time of the selected hour.
